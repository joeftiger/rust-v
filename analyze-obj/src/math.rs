#[derive(Copy, Clone, Debug)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vec3 {
    #[inline]
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }

    #[inline]
    pub fn broadcast(v: f64) -> Self {
        Self::new(v, v, v)
    }

    #[inline]
    pub fn inf() -> Self {
        Self::broadcast(f64::INFINITY)
    }

    #[inline]
    pub fn neg_inf() -> Self {
        Self::broadcast(f64::NEG_INFINITY)
    }

    #[inline]
    pub fn min(self, other: Self) -> Self {
        Self::new(
            self.x.min(other.x),
            self.y.min(other.y),
            self.z.min(other.z),
        )
    }

    #[inline]
    pub fn max(self, other: Self) -> Self {
        Self::new(
            self.x.max(other.x),
            self.y.max(other.y),
            self.z.max(other.z),
        )
    }

    pub fn from_str(s: &str) -> Option<Self> {
        let mut split = s.split_whitespace();

        Some(Self::new(
            split.next()?.parse().ok()?,
            split.next()?.parse().ok()?,
            split.next()?.parse().ok()?
        ))
    }
}

impl core::ops::Mul<f64> for Vec3 {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(
            self.x * rhs,
            self.y * rhs,
            self.z * rhs,
        )
    }
}