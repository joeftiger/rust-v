mod obj;
mod math;

fn main() -> Result<(), String> {
    let file = std::env::args()
        .skip(1)
        .next()
        .expect("No .obj file given");

    let scale = std::env::args()
        .skip(2)
        .next();

    let obj = obj::Obj::load(&file)?;
    println!("min: {:?}", obj.min);
    println!("max: {:?}", obj.max);

    if let Some(scale) = scale {
        let scale = scale.parse().map_err(|_| "Unable to parse <scale>")?;

        println!("------- SCALED {} -------", scale);
        println!("min: {:?}", obj.min * scale);
        println!("max: {:?}", obj.max * scale);
    }

    Ok(())
}
