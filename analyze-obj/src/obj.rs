use crate::math::Vec3;

#[derive(Copy, Clone, Debug)]
pub struct Obj {
    pub min: Vec3,
    pub max: Vec3,
}

impl Obj {
    #[inline]
    pub fn new(min: Vec3, max: Vec3) -> Self {
        Self { min, max }
    }

    pub fn load(path: &str) -> Result<Self, String> {
        let file = std::fs::read_to_string(path).map_err(|e| e.to_string())?;

        let mut min = Vec3::inf();
        let mut max = Vec3::neg_inf();

        for (number, line) in file.lines().enumerate() {
            if line.starts_with('#') || line.is_empty() {
                continue;
            }

            let (key, value) = line.split_once(' ')
                .ok_or(format!("Cannot parse line: {}", number))?;
            match key {
                "v" => {
                    let v = Vec3::from_str(value)
                        .ok_or(format!("Cannot parse key 'v' at line: {}", number))?;

                    min = min.min(v);
                    max = max.max(v);
                }
                _ => (),
            }
        }

        Ok(Self::new(min, max))
    }
}